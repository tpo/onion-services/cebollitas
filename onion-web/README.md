# Servicio web

Para crear y ejecutar la imagen web:

    docker build . -t web
    docker run -itd --name web -p 8000:80 web

Para acceder al contenedor:

    docker exec -it web /bin/bash

Para obtener la dirección del servicio cebolla:

    docker exec -it web /bin/cat /var/lib/tor/web/hostname

Para terminar el servicio:

    docker stop web

Para remover el servicio:

    docker rm web
