#!/usr/bin/env bash

# Empeza el servicio web
service nginx start

# Empeza el servicio Tor
service tor start
