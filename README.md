# Cebollitas!

## Versión en Español

Projecto con ejemplos para aprender a crear [Servicios Cebolla][].

### Informaciones generales

Este projecto contiene ejemplos útiles para crear
Servicios Cebolla usando la tecnología [Docker][]:

* [onion-app](onion-app): ejemplo de aplicación web.
* [onion-web](onion-web): ejemplo de servicio web.
* [onion-ssh](onion-ssh): ejemplo de acesso [SSH][].

### Requerimientos generales

0. Desacargar el contenido del repositorio.
1. Instalar [Docker][].
2. Instalar [docker-compose][].
3. Instalar el [Tor Browser][].

[Servicios Cebolla]: https://community.torproject.org/es/onion-services/
[Docker]: https://docs.docker.com
[docker-compose]: https://docs.docker.com/compose/
[SSH]: https://es.wikipedia.org/wiki/Secure_Shell
[Tor Browser]: https://www.torproject.org/es/download/
