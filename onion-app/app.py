#!/usr/bin/env python3
#
# Aplicación web de ejemplo.
#

from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return """
    <html>
        <head>
            <title>Cebollita</title>
        </head>

        <body>
            <h1>Cebollita te saluda!</h1>
        </body>
    </html>
    """
