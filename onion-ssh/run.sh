#!/usr/bin/env bash

# Empeza el servicio SSH
service ssh start

# Empeza el servicio Tor
service tor start
