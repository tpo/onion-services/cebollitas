# Tor y SSH

Para crear y ejecutar la imagen:

    docker build . -t ssh
    docker run -itd --name ssh -p 2222:22 ssh

Para acceder al contenedor:

    docker exec -it ssh /bin/bash

Para obtener la dirección del servicio cebolla:

    docker exec -it ssh /bin/cat /var/lib/tor/ssh/hostname
